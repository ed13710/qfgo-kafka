package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"

	"github.com/quickfixgo/quickfix"
	log "github.com/sirupsen/logrus"
	"gitlab.com/ed13710/qfgo-kafka/cmd"
)

func main() {

	file, err := os.OpenFile("info.log", os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	log.SetOutput(file)
	log.SetFormatter(&log.JSONFormatter{})
	log.SetLevel(log.WarnLevel)

	log.Print("Starting...")

	flag.Parse()
	cfgFileName := flag.Arg(0)

	//FooApplication is your type that implements the Application interface
	app := cmd.NewBridgeApplication("192.168.10.35:9092")

	cfg, err := os.Open(cfgFileName)
	if err != nil {
		fmt.Printf("Error opening %v, %v\n", cfgFileName, err)
		return
	}

	appSettings, err := quickfix.ParseSettings(cfg)
	if err != nil {
		fmt.Println("Error reading cfg,", err)
		return
	}

	storeFactory := quickfix.NewMemoryStoreFactory()
	logFactory := quickfix.NewScreenLogFactory()
	acceptor, err := quickfix.NewAcceptor(app, storeFactory, appSettings, logFactory)

	if err != nil {
		fmt.Printf("Unable to create Acceptor: %s\n", err)
		return
	}

	err = acceptor.Start()
	if err != nil {
		fmt.Printf("Unable to start Acceptor: %s\n", err)
		return
	}
	fmt.Printf("Acceptor started\n")

	interrupt := make(chan os.Signal)
	signal.Notify(interrupt, os.Interrupt, os.Kill)
	<-interrupt

	fmt.Printf("Stopping Acceptor\n")
	acceptor.Stop()
}
