module gitlab.com/ed13710/qfgo-kafka

require (
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/quickfixgo/quickfix v0.6.0
	github.com/segmentio/kafka-go v0.2.2
	github.com/shopspring/decimal v0.0.0-20180709203117-cd690d0c9e24 // indirect
	github.com/sirupsen/logrus v1.3.0
	github.com/stretchr/testify v1.3.0 // indirect
)
