package cmd

import (
	"context"
	"log"
	"time"

	"github.com/quickfixgo/quickfix"
	"github.com/segmentio/kafka-go"
	//"github.com/segmentio/kafka-go/snappy"
)

func configure(kafkaBrokerUrls []string, clientID string, topic string) (w *kafka.Writer, err error) {
	dialer := &kafka.Dialer{
		Timeout:  10 * time.Second,
		ClientID: clientID,
	}

	config := kafka.WriterConfig{
		Brokers:          kafkaBrokerUrls,
		Topic:            topic,
		Balancer:         &kafka.LeastBytes{},
		Dialer:           dialer,
		WriteTimeout:     10 * time.Second,
		ReadTimeout:      10 * time.Second,
		Async: true,
		//CompressionCodec: snappy.NewCompressionCodec(),
	}
	w = kafka.NewWriter(config)
	return w, nil
}


//BridgeApplication is the default applicatino
type BridgeApplication struct {
	//KafkaURL to connect to kafka
	KafkaURL                 string
	kafkaConnectionBySession map[quickfix.SessionID]*kafka.Writer
	//kafkaConnectionBySession := make(map[quickfix.SessionID]*Dialer)
}




// NewBridgeApplication
func NewBridgeApplication(kafkaURL string) *BridgeApplication{
    m := new(BridgeApplication)
    m.KafkaURL = kafkaURL
    m.kafkaConnectionBySession = make(map[quickfix.SessionID]*kafka.Writer)
    return m
}

// OnCreate does
func (b BridgeApplication) OnCreate(sessionID quickfix.SessionID) {
	topic := sessionID.SenderCompID + "_" + sessionID.TargetCompID  + "_1"
	log.Print("Creating kafka connection for " + topic)
	writer, err := configure([]string {b.KafkaURL}, "test", topic)
	if err != nil {
		log.Fatal("Could not create the Kafka connection ", err)
	}
	log.Print("Created kafka connection for " + topic)
	b.kafkaConnectionBySession[sessionID] = writer

	go func() {

			// make a new reader that consumes from topic-A
		config := kafka.ReaderConfig{
			Brokers:         []string {b.KafkaURL},
			GroupID:         "kafkaClientId",
			Topic:           topic,
			MinBytes:        1,            // 1KB
			MaxBytes:        10e6,            // 10MB
			MaxWait:         1 * time.Second, // Maximum amount of time to wait for new data to come when fetching batches of messages from kafka.
			//ReadLagInterval: -1,
		}

		reader := kafka.NewReader(config)
		defer reader.Close()

		for {
			log.Print("going to read messages")
			//ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			//defer cancel()

			m, err := reader.ReadMessage(context.Background())
			if err != nil {
				log.Printf("error while receiving message: %s", err.Error())
				continue
			}
			log.Print("read a messages")
			now := time.Now()
			value := m.Value
			//if m.CompressionCodec == snappy.NewCompressionCodec() {
			//	_, err = snappy.NewCompressionCodec().Decode(value, m.Value)
			//}

			if err != nil {
				log.Printf("error while receiving message: %s", err.Error())
				continue
			}
			roundTripTime := now.Sub(m.Time)
			log.Printf("message at topic/partition/offset %v/%v/%v @ %v: %s\n", m.Topic, m.Partition, m.Offset, m.Time.Format(time.RFC3339Nano),  string(value))
			log.Printf("message roundtrip %v\n", roundTripTime )
		}
	}()
}

// OnLogon does
func (b BridgeApplication) OnLogon(sessionID quickfix.SessionID) { return }

// OnLogout does
func (b BridgeApplication) OnLogout(sessionID quickfix.SessionID) {
	return
}

// FromApp does
func (b BridgeApplication) FromApp(msg *quickfix.Message, sessionID quickfix.SessionID) quickfix.MessageRejectError {
	writer := b.kafkaConnectionBySession[sessionID]
	seqNum, err := msg.Header.GetInt(quickfix.Tag(34))
	if err != nil {
		return err
	}

	writer.WriteMessages(context.Background(), 
		kafka.Message{
			Key: []byte{byte(seqNum)},
			Value: []byte(msg.String()),
    		Time:  time.Now(),
		})
    log.Print("Sending Message on the bus")
	return nil
}

// ToApp does
func (b BridgeApplication) ToApp(msg *quickfix.Message, sessionID quickfix.SessionID) error {
	return nil
}

// ToAdmin does
func (b BridgeApplication) ToAdmin(msg *quickfix.Message, sessionID quickfix.SessionID) { return }

// FromAdmin does
func (b BridgeApplication) FromAdmin(msg *quickfix.Message, sessionID quickfix.SessionID) quickfix.MessageRejectError {
	return nil
}
