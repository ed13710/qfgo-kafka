package main

import (
	"os"
	"context"
	"strings"
"time"	
	"fmt"
	"github.com/segmentio/kafka-go"
	//"github.com/quickfixgo/quickfix"
	log "github.com/sirupsen/logrus"
)

func main() {
	log.Print("PreStarting")
	file, err := os.OpenFile("info.log", os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
		return
	}

	defer file.Close()
	log.Print("Log initialzed")

	//log.SetOutput(file)
	//log.SetFormatter(&log.JSONFormatter{})
	//log.SetLevel(log.WarnLevel)

	log.Print("Starting...")



	brokers := strings.Split("192.168.10.35:19092", ",")

	// make a new reader that consumes from topic-A
	config := kafka.ReaderConfig{
		Brokers:         brokers,
		//GroupID:         kafkaClientId,
		Topic:           "TW_ISLD",
		MinBytes:        10e3,            // 10KB
		MaxBytes:        10e6,            // 10MB
		MaxWait:         1 * time.Second, // Maximum amount of time to wait for new data to come when fetching batches of messages from kafka.
		ReadLagInterval: -1,
	}

	reader := kafka.NewReader(config)
	defer reader.Close()

	for {
		m, err := reader.ReadMessage(context.Background())
		if err != nil {
			log.Error("error while receiving message: ")
			log.Error(err)
			continue
		}

		value := m.Value

		if err != nil {
			log.Error("error while receiving message: " )
			log.Error(err)
			continue
		}

		fmt.Printf("message at topic/partition/offset %v/%v/%v: %s\n", m.Topic, m.Partition, m.Offset, string(value))
	}


    }
    
